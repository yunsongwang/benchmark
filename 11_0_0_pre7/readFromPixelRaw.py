import FWCore.ParameterSet.Config as cms


process = cms.Process('READ')

process.load('FWCore.MessageService.MessageLogger_cfi')
process.MessageLogger.cerr.FwkReport.reportEvery = 100

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(4200)
)

process.options = cms.untracked.PSet()

# Setup CMSSW for multithreading
process.options.numberOfThreads = cms.untracked.uint32(1)
process.options.numberOfStreams = cms.untracked.uint32(0)

process.load('sourceFromPixelRaw_cff')

process.out = cms.OutputModule("AsciiOutputModule",
    outputCommands = cms.untracked.vstring(
        "keep *_rawDataCollector_*_*",
    ),
    verbosity = cms.untracked.uint32(0),
)

process.p = cms.EndPath(process.out)
