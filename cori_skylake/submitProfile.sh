#!/bin/bash

#SBATCH -C gpu
#SBATCH --nodes=1
#SBATCH --exclusive
#SBATCH --gres=gpu:1
#SBATCH -t 04:00:00
#SBATCH --image=docker:makortel/cmssw_patatrack:11_0_0_pre7_v1
#SBATCH --module=cvmfs
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=matti@fnal.gov

srun -n 1 -c 80 shifter /global/homes/m/matti/nesap/benchmark/cori_skylake/runProfile.sh
