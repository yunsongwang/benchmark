#!/usr/bin/env python3

import re
import sys
import math
import statistics

def seconds(m):
    return ( float(m.group("hour"))*60 + float(m.group("min")) )*60 + float(m.group("sec"))

def main(files):
    nevents = []
    throughput = []
    record_re = re.compile("Begin processing the (?P<record>\d+)[snr][td] record.*on stream.*(?P<hour>\d\d):(?P<min>\d\d):(?P<sec>\d\d\.\d\d\d)")
    for name in files:
        with open(name) as f:
            skipped = False
            startRecord = None
            stopRecord = None
            start = None
            stop = None
            for line in f:
                m = record_re.search(line)
                if m:
                    if start is None:
                        # Skip the "1st" record printout
                        if not skipped:
                            skipped = True
                            continue
                        startRecord = int(m.group("record"))
                        start = seconds(m)
                    else:
                        stopRecord = int(m.group("record"))
                        stop = seconds(m)
            if start is None or stop is None:
                raise Exception("Did not find times from file %s" % name)
            nev = stopRecord-startRecord
            nevents.append(nev)
            throughput.append(float(nev)/(stop-start))

    for t, nev in zip(throughput, nevents):
        print("Throughput %f (over %d events)" % (t, nev))
    if len(throughput) > 1:
        print("Mean %f +- %f" % (statistics.mean(throughput), statistics.stdev(throughput)/math.sqrt(len(throughput))))


if __name__ == "__main__":
    main(sys.argv[1:])
